package lib.ui;

import lib.feature.common.GlobalParameters;
import lib.report.MyLogger;
import lib.ui.exceptions.BrowserException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shaman on 17.11.2015.
 */
public class Browser implements WrapsDriver {

    public static final int COMMON_ELEMENT_WAIT_TIME_OUT = 25;
    public static final int IMPLYCITY_TIME_OUT = 3;
    public static final int ELEMENT_THERE_CHECK_TIME_OUT = 2;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;

    private static Map<Thread, Browser> instances = new HashMap<>();
    private WebDriver driver;

    private Browser() {
    }

    public static synchronized Browser rise() {
        Browser browser = new Browser();
        browser.createDriver();
        instances.put(Thread.currentThread(), browser);
        return browser;
    }

    public static synchronized Browser current() {
        return instances.get(Thread.currentThread());
    }

    private static String screenshotName() {
        return GlobalParameters.getInstance().getResultDir()
                + File.separator + System.nanoTime() + ".png";
    }

    public static void screenshot() {
        try {
            byte[] scrFileBytes = ((TakesScreenshot)
                    instances.get(Thread.currentThread()).getWrappedDriver())
                    .getScreenshotAs(OutputType.BYTES);
            File screenshotFile = new File(screenshotName());
            FileUtils.writeByteArrayToFile(screenshotFile, scrFileBytes);
            MyLogger.save(screenshotFile.getName());
        } catch (IOException e) {
            e.printStackTrace();
            MyLogger.error("Failed to write screenshoot " + e.getMessage(), e);
        }
    }

    public void close() {
        MyLogger.info("Try to close driver");
        getWrappedDriver().quit();
        instances.put(Thread.currentThread(), null);
    }

    public void refresh() {
        MyLogger.info("Refresh");
        getWrappedDriver().navigate().refresh();
    }

    private WebDriver createDriver() {
        if (GlobalParameters.getInstance().getSeleniumHub() != null) {
            switch (GlobalParameters.getInstance().getBrowserType()) {
                default:
                case FIREFOX:
                    MyLogger.debug("Start firefox remote browser");
                    driver = prepareRemoteFirefoxDriver();
                    break;
                case CHROME:
                    MyLogger.debug("Start chrome remote browser");
                    driver = prepareRemoteChromeDriver();
                    break;
            }
        } else {
            switch (GlobalParameters.getInstance().getBrowserType()) {
                default:
                case FIREFOX:
                    MyLogger.debug("Start firefox local browser");
                    driver = prepareLocalFirefoxDriver();
                    break;
                case CHROME:
                    MyLogger.debug("Start chrome local browser");
                    driver = prepareLocalChromeDriver();
                    break;
            }
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(IMPLYCITY_TIME_OUT,
                TimeUnit.SECONDS);
        return driver;
    }

    private RemoteWebDriver createRemote(DesiredCapabilities capabilities) {
        RemoteWebDriver driver;
        try {
            driver = new RemoteWebDriver(new URL(GlobalParameters.getInstance().getSeleniumHub()), capabilities);
        } catch (MalformedURLException e) {
            throw new BrowserException("URL to remote has not valid format : " + e.getMessage(), e);
        }
        return driver;
    }

    private WebDriver prepareRemoteFirefoxDriver() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir", GlobalParameters.getInstance()
                .getDownloadPath());
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        return createRemote(capabilities);
    }

    private WebDriver prepareRemoteChromeDriver() {
        HashMap<String, Object> chromeOptions = new HashMap<>();
        chromeOptions.put("profile.default_content_settings.popups", 0);
        chromeOptions.put("download.prompt_for_download", "false");
        chromeOptions.put("download.default_directory",
                GlobalParameters.getInstance().getDownloadPath());
        chromeOptions.put("download.directory_upgrade", true);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromeOptions);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return createRemote(capabilities);
    }

    private WebDriver prepareLocalFirefoxDriver() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir", GlobalParameters.getInstance()
                .getDownloadPath());
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        driver = new FirefoxDriver(capabilities);
        return driver;
    }

    private WebDriver prepareLocalChromeDriver() {

        HashMap<String, Object> chromeOptions = new HashMap<>();
        chromeOptions.put("profile.default_content_settings.popups", 0);
        chromeOptions.put("download.prompt_for_download", "false");
        chromeOptions.put("download.default_directory",
                GlobalParameters.getInstance().getDownloadPath());
        chromeOptions.put("download.directory_upgrade", true);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromeOptions);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        System.setProperty("webdriver.chrome.driver", GlobalParameters.getInstance()
                .getChromeDriverPath());
        driver = new ChromeDriver(capabilities);
        return driver;
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String url) {
        MyLogger.debug("Open page : " + url);
        driver.get(url);
    }

    public boolean elementIsOnThisPage(By locator) {
        MyLogger.debug("Wait for appear of : " + locator);
        boolean result = true;
        try {
            new WebDriverWait(getWrappedDriver(), ELEMENT_THERE_CHECK_TIME_OUT).
                    until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
            result = false;
            MyLogger.debug("There is no " + locator + " element on this page");
        }
        return result;
    }

    public void dragAndDropFile(By locatorWhat, By locatorTo) {
        WebElement file = Browser.current().
                waitForElementIsClickable(locatorWhat);
        WebElement destination = Browser.current().
                waitForElementIsClickable(locatorTo);
        Action action = new Actions(getWrappedDriver()).dragAndDrop(file, destination).build();
        action.perform();
    }

    public void waitUntilElementHide(By locator) {
        MyLogger.debug("Wait until hide : " + locator);
        elementIsOnThisPage(locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.
                invisibilityOfElementLocated(locator));
    }

    public WebElement waitForElementIsClickable(By locator) {
        MyLogger.debug("Wait for clickable of : " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.
                elementToBeClickable(locator));
        return getWrappedDriver().findElement(locator);
    }

    public void writeTextClickable(By locator, String text) {
        MyLogger.debug("Write text to : " + locator + " text = '" + text + "'");
        waitForElementIsClickable(locator).sendKeys(text);
    }

    public void writeText(By locator, String text) {
        elementIsOnThisPage(locator);
        MyLogger.debug("Write text to : " + locator + " text = '" + text + "'");
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void click(By locator) {
        waitForElementIsClickable(locator).click();
        MyLogger.debug("Click to : " + locator);
    }

    public void submit(By locator) {
        waitForElementIsClickable(locator).submit();
        MyLogger.debug("Submit : " + locator);
    }

    public String getText(By locator) {
        MyLogger.debug("Get text from : " + locator);
        elementIsOnThisPage(locator);
        return getWrappedDriver().findElement(locator).getText();
    }

    public String getAttribute(By locator, String attribute) {
        MyLogger.debug("Get attribute '" + attribute + "' from : " + locator);
        return waitForElementIsClickable(locator).getAttribute(attribute);
    }
}
