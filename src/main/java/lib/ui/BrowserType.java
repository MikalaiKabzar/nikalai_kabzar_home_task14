package lib.ui;

/**
 * Created by Shaman on 26.01.2016.
 */
public enum BrowserType {

    FIREFOX("firefox"),
    CHROME("chrome");

    String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
