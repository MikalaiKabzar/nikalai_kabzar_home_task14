package lib.ui.exceptions;

/**
 * Created by Konstantsin_Simanenk on 1/26/2016.
 */
public class BrowserException extends RuntimeException {
    
    public BrowserException(String message) {
        super(message);
    }

    public BrowserException(String message, Throwable cause) {
        super(message, cause);
    }
}
