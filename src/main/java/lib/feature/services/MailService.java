package lib.feature.services;

import lib.feature.models.letter.Letter;
import lib.feature.screen.mail.BasePage;
import lib.feature.screen.mail.DraftPage;
import lib.feature.screen.mail.IncomingPage;
import lib.feature.screen.mail.PrepareLetterPage;
import lib.report.MyLogger;
import lib.ui.Browser;
import org.openqa.selenium.By;

import static lib.feature.screen.mail.BasePage.FIRST_LETTER_LOCATOR;

/**
 * Created by Shaman on 14.01.2016.
 */
public class MailService {

    public static void deleteLetter(Letter letter) {
        selectLetter(letter);
        Browser.screenshot();
        DraftPage.deleteSelectedLetter();
        Browser.screenshot();
    }

    public static void sentLetter(Letter letter) {
        trySentLetter(letter);
        PrepareLetterPage.waitForDoneFrame()
                .openIncoming()
                .waitUntilStatuslineHide()
                .openIncoming();
        Browser.screenshot();
    }

    public static void trySentLetter(Letter letter) {
        prepareLetter(letter);
        PrepareLetterPage.sentMailButtonClick();
        Browser.screenshot();
    }

    public static void prepareLetter(Letter letter) {
        IncomingPage.writeLetter()
                .prepareLetter(letter);
        Browser.screenshot();
    }

    public static void makeDraftLetter(Letter letter) {
        prepareLetter(letter);
        PrepareLetterPage.openDraft();
        PrepareLetterPage.confirmSaveDraft();
        findLetterId(letter);
        Browser.screenshot();
    }

    public static void findLetterId(Letter letter) {
        MyLogger.debug("Find letter id");
        String tempString = Browser.current().getAttribute(
                By.xpath("//div[@class = 'block-messages-wrap' and not(@style='display: none;')]" +
                        "//span[@title = '" + letter.getSubject() + "']/ancestor::a"), "href");
        letter.setId(tempString.substring(tempString.lastIndexOf("/") + 1));
        MyLogger.info("Letter id is:" + letter.getId());
    }

    public static void selectLetter(Letter letter) {
        MyLogger.debug("Select letter :" + letter.getId());
        Browser.screenshot();
        Browser.current().click(By.xpath("//input[@value = '" + letter.getId() + "']"));
    }

    public static boolean checkLetterIsHere(Letter letter) {
        MyLogger.debug("Check letter '" + letter.getId() + "' is here");
        Browser.screenshot();
        return Browser.current().elementIsOnThisPage(
                By.xpath("//div[@data-id = '" + letter.getId() + "']"));
    }

    public static boolean checkLetterIsInTrash(Letter letter) {
        IncomingPage.openTrash();
        return checkLetterIsHere(letter);
    }

    public static boolean checkCurrentLetterIsHere(Letter letter) {
        MyLogger.debug("Check letter " + letter.getId() + " is here");
        Browser.screenshot();
        return Browser.current().elementIsOnThisPage(
                By.xpath("//div[not(@style = 'display: none;')]/" +
                        "div[@class = 'b-messages b-messages_threaded']/" +
                        "div[@data-id = 't" + letter.getId() + "']"));
    }

    public static boolean checkCurrentLetterInOutBox(Letter letter) {
        BasePage.openOutcoming();
        Browser.current().waitForElementIsClickable(BasePage.OUTCOMING_IS_OPEN_LOCATOR);
        return checkCurrentLetterIsHere(letter);
    }

    public static boolean checkEmptyLetterIsHere(Letter letter) {
        MyLogger.debug("Check empty letter is here");

        if (Browser.current().getWrappedDriver()
                .findElement(FIRST_LETTER_LOCATOR)
                .getAttribute("class")
                .lastIndexOf("message_without-folder") > 1) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkEmptyLetterInOutBox(Letter letter) {
        BasePage.openOutcoming();
        Browser.current().waitForElementIsClickable(BasePage.OUTCOMING_IS_OPEN_LOCATOR);
        return checkEmptyLetterIsHere(letter);
    }

}
