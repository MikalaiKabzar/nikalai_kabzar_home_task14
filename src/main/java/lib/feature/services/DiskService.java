package lib.feature.services;

import lib.feature.common.GlobalParameters;
import lib.feature.models.textFile.TextFile;
import lib.feature.screen.disk.YandexDiskPage;
import lib.feature.screen.disk.YandexTrashPage;
import lib.feature.screen.mail.BasePage;
import lib.report.MyLogger;
import lib.ui.Browser;
import lib.util.Content;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import tests.BaseDiskTestClass;

import java.io.File;
import java.util.List;

import static lib.feature.screen.disk.YandexDiskPage.*;

/**
 * Created by Shaman on 16.01.2016.
 */
public class DiskService {

    public static final int DOWNLOAD_TIMEOUT = 20;

    public static void prepareYandexDiskFilesFolders(List<TextFile> filesForTest) {
        MyLogger.info("Prepare YandexDisk files and folders");
        new BasePage().openDisk().openTrash().clearYandexDiskTrash().openFilesPage();
        for (int i = 1; i <= filesForTest.size(); i++) {
            YandexDiskPage.upload(BaseDiskTestClass.getFileName(i));
        }
        Browser.current().refresh();

    }

    public static void uploadFile(String... fileName) {
        BasePage.openDisk();
        for (int i = 0; i < fileName.length; i++) {
            MyLogger.info("Upload file " + fileName[i]);
            YandexDiskPage.upload(fileName[i]);
        }
        Browser.current().refresh();
    }

    public static void downloadFile(String... fileName) {
        for (int i = 0; i < fileName.length; i++) {
            MyLogger.info("Download file " + fileName[i]);
            File file = new File(GlobalParameters.getInstance().getDownloadPath() + fileName[i]);
            if (file.exists()) {
                file.delete();
            }
            YandexDiskPage.downloadFile(fileName[i]);
        }
    }

    public static boolean checkDownloadedFile(String... fileName) {
        Boolean allFileHasCorrectContent = true;
        for (int i = 0; i < fileName.length; i++) {
            String fullFileName = GlobalParameters.getInstance().getDownloadPath() + fileName[i];
            MyLogger.debug("Check. Downloaded file has correct content : " + fullFileName);
            File file = new File(fullFileName);
            if (!Content.getContentByFileName(fileName[i]).equals(Content.readFile(file))) {
                MyLogger.error("Downloaded file has incorrect content " + fullFileName);
                allFileHasCorrectContent = false;
            }
        }
        return allFileHasCorrectContent;
    }

    public static boolean checkCorrectDownloadFile(String... fileName) {
        Boolean allFileCorrectDownloaded = true;
        for (int i = 0; i < fileName.length; i++) {
            String fullFileName = GlobalParameters.getInstance().getDownloadPath() + fileName[i];
            MyLogger.info("Check correct download file " + fullFileName);
            File file = new File(fullFileName);
            if (!file.exists()) {
                int timer = 0;
                while (!file.exists() && timer < DOWNLOAD_TIMEOUT * 2) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    timer++;
                }
            }
            if (!file.exists()) {
                allFileCorrectDownloaded = false;
                MyLogger.error("There is no downloaded file  " + fullFileName);
            }
        }
        return allFileCorrectDownloaded;
    }

    public static boolean checkFilesNotHere(String... fileName) {
        Boolean allFileIsNotHere = true;
        for (int i = 0; i < fileName.length; i++) {
            if (Browser.current().elementIsOnThisPage(getFileXpath(fileName[i]))) {
                MyLogger.error("There is file : " + fileName[i]);
                allFileIsNotHere = false;
            }
        }
        return allFileIsNotHere;
    }

    public static boolean checkFilesHere(String... fileName) {
        Boolean allFileIsHere = true;
        for (int i = 0; i < fileName.length; i++) {
            if (!Browser.current().elementIsOnThisPage(getFileXpath(fileName[i]))) {
                MyLogger.error("There is no file : " + fileName[i]);
                allFileIsHere = false;
            }
        }
        return allFileIsHere;
    }

    public static boolean checkFilesInTrash(String... fileName) {
        YandexDiskPage.openTrash();
        return checkFilesHere(fileName);
    }

    public static boolean checkFilesInDisk(String... fileName) {
        YandexTrashPage.openFilesPage();
        return checkFilesHere(fileName);
    }

    public static void deleteFilesToTrash(String... fileName) {
        for (int i = 0; i < fileName.length; i++) {
            MyLogger.debug("Delete file to trash : " + fileName[i]);
            YandexDiskPage.deleteFileToTrash(fileName[i]);
        }
    }

    public static void deleteFilesPermanently(String... fileName) {
        for (int i = 0; i < fileName.length; i++) {
            MyLogger.debug("Delete file permanently : " + fileName[i]);
            YandexTrashPage.deleteFilePermanently(fileName[i]);
        }
    }

    public static void restoreFiles(String... fileName) {
        for (int i = 0; i < fileName.length; i++) {
            MyLogger.debug("Restore file : " + fileName[i]);
            YandexTrashPage.restoreFile(fileName[i]);
        }
    }

    public static void deleteFilesCtrlClick(String... fileName) {
        MyLogger.debug("Delete files using Ctrl+Click");
        Actions actions = new Actions(Browser.current().getWrappedDriver());
        actions.keyDown(Keys.CONTROL);
        for (int i = 0; i < fileName.length; i++) {
            actions.click(Browser.current().
                    waitForElementIsClickable(getFileXpath(fileName[i])));
        }
        actions.keyUp(Keys.CONTROL).build().perform();
        Browser.current().dragAndDropFile(getFileXpath(fileName[0]),
                FILE_TO_TRASH_BUTTON_LOCATOR);
        Browser.current().waitUntilElementHide(PROGRESSBAR_LOCATOR);
    }

}
