package lib.feature.services;

import lib.feature.models.account.Account;
import lib.feature.screen.LoginPage;
import lib.report.MyLogger;
import lib.ui.Browser;

import static lib.feature.common.CommonConstants.BASE_URL;
import static lib.feature.screen.mail.BasePage.HEADER_USER_NAME_LOCATOR;
import static lib.ui.Browser.screenshot;

/**
 * Created by Shaman on 14.01.2016.
 */
public class LoginService {

    static LoginPage loginPage = new LoginPage();

    public static void login(Account account) {
        MyLogger.info("Login (login: " + account.getLogin() + ", password: " +
                account.getPassword() + ")");
        loginPage.openUrl(BASE_URL)
                .openLoginForm()
                .setLogin(account.getLogin())
                .setPassword(account.getPassword());
        screenshot();
        loginPage.clickSubmit();
        screenshot();
    }

    public static boolean checkLoginPositive(Account account) {
        return Browser.current().getText(HEADER_USER_NAME_LOCATOR).toLowerCase()
                .equals(account.getLogin().toLowerCase());
    }

}
