package lib.feature.common;

import lib.runner.Runner;
import lib.ui.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlSuite.ParallelMode;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by Shaman on 16.01.2016.
 */
public class GlobalParameters {

    private static GlobalParameters instance;

    @Option(name = "-bt", usage = "browser type: firefox or chrome")
    private BrowserType browserType = BrowserType.CHROME;
    @Option(name = "-suites", usage = "list of pathes to suites",
            handler = StringArrayOptionHandler.class, required = true)
    private List<String> suites = null;
    @Option(name = "-pm", usage = "parallel mode: false or tests")
    private XmlSuite.ParallelMode parallelMode = XmlSuite.ParallelMode.FALSE;
    @Option(name = "-tc", usage = "amount of threads for parallel execution")
    private int threadCount = 0;
    @Option(name = "-hub", usage = "selenium hub")
    private String seleniumHub = null;
    @Option(name = "-crdp", usage = "Chrome driver path")
    private String chromeDriverPath = "./src/main/resources/chromedriver_win.exe";
    @Option(name = "-result_dir", usage = "Directory to put results")
    private String resultDir = "results";

    private String tempFolderPath = getJarDir()+"tempFolderForTests";
    public String uploadPath;
    public String downloadPath;

    public static GlobalParameters getInstance() {
        if (instance == null) {
            instance = new GlobalParameters();
        }
        return instance;
    }

    public String getChromeDriverPath() {
        return chromeDriverPath;
    }

    public void setChromeDriverPath(String chromeDriverPath) {
        this.chromeDriverPath = chromeDriverPath;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public String getSeleniumHub() {
        return seleniumHub;
    }

    public void setSeleniumHub(String seleniumHub) {
        this.seleniumHub = seleniumHub;

    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public String getUploadPath() {
        return tempFolderPath + "\\Upload\\";
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getDownloadPath() {
        return tempFolderPath + "\\Download\\";
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public String getTempFolderPath() {
        return this.getInstance().tempFolderPath;
    }

    public void setTempFolderPath(String tempFolderPath) {
        this.getInstance().tempFolderPath = tempFolderPath;
    }

    public String getResultDir() {
        return resultDir;
    }

    public void setResultDir(String resultDir) {
        this.resultDir = resultDir;
    }

    public static String getJarDir() {
        String decodedPath = "";
        String path = Runner.class.getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath();
        try {
            decodedPath = URLDecoder.decode(path, "UTF-8").replace("/","\\").substring(1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return decodedPath;
    }

    @Override
    public String toString() {
        return "GlobalConfig{" +
                "browserType=" + browserType +
                ", suites=" + suites +
                ", parallelMode=" + parallelMode +
                ", threadCount=" + threadCount +
                ", seleniumHub='" + seleniumHub + '\'' +
                ", resultDir='" + resultDir + '\'' +
        '}';
    }
}
