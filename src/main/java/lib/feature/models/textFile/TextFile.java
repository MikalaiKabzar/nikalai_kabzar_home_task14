package lib.feature.models.textFile;

/**
 * Created by Shaman on 25.01.2016.
 */
public class TextFile {
    private String name;
    private String content;
    private String address;

    public TextFile(String name, String content, String address) {
        this.name = name;
        this.content = content;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
