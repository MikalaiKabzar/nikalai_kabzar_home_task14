package lib.feature.models.textFile;

import lib.feature.common.GlobalParameters;

/**
 * Created by Shaman on 25.01.2016.
 */
public class TextFileBuilder {

    public static TextFile getTextFile(int nomber) {
        String name = "TestFile-" + nomber + ".txt";
        String content = "This is file (" + nomber + ") for tests.";
        String address = GlobalParameters.getInstance().getUploadPath();
        return new TextFile(name, content, address);
    }
}
