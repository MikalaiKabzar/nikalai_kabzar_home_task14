package lib.feature.models.letter;

/**
 * Created by Shaman on 03.01.2016.
 */
public class Letter {

    private String to;

    private String subject;

    private String body;

    private String id;

    public Letter(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
