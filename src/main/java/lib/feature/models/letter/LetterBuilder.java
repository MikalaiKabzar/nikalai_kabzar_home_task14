package lib.feature.models.letter;

import lib.feature.common.CommonConstants;

import java.util.Date;

/**
 * Created by Shaman on 03.01.2016.
 */
public class LetterBuilder {

    private static String getRandoomNumberInString() {
        return "___"+Math.random()*100000000;
    }

    public static Letter getRandomLetter() {
        String mailTo = CommonConstants.USER_LOGIN;
        String mailSubject = "test subject " + new Date(System.currentTimeMillis())+getRandoomNumberInString();
        String mailContent = "mail content " + new Date(System.currentTimeMillis())+getRandoomNumberInString();
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getWrongAddressLetter() {
        String mailTo = CommonConstants.USER_LOGIN+"@";
        String mailSubject = "test subject " + new Date(System.currentTimeMillis())+getRandoomNumberInString();
        String mailContent = "mail content " + new Date(System.currentTimeMillis())+getRandoomNumberInString();
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getEmptyAddressLetter() {
        String mailTo = "";
        String mailSubject = "test subject " + new Date(System.currentTimeMillis())+getRandoomNumberInString();
        String mailContent = "mail content " + new Date(System.currentTimeMillis())+getRandoomNumberInString();
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getEmptyContentLetter() {
        String mailTo = CommonConstants.USER_LOGIN;
        String mailSubject = "";
        String mailContent = "";
        return new Letter(mailTo, mailSubject, mailContent);
    }

}
