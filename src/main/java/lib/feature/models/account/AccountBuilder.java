package lib.feature.models.account;

import static lib.feature.common.CommonConstants.*;

/**
 * Created by Shaman on 13.01.2016.
 */
public class AccountBuilder {

    public static Account getDefaultAccount() {
        String login = USER_LOGIN;
        String password = USER_PASSWORD;
        String email = login;
        return new Account(login, password, email);
    }

    public static Account getWrongPasswordAccount() {
        String login = USER_LOGIN;
        String password = WRONG_USER_PASSWORD;
        String email = login;
        return new Account(login, password, email);
    }

    public static Account getNonexistentAccount() {
        String login = WRONG_USER_LOGIN;
        String password = USER_PASSWORD;
        String email = login;
        return new Account(login, password, email);
    }

}
