package lib.feature.screen.disk;

import lib.feature.common.GlobalParameters;
import lib.report.MyLogger;
import lib.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Shaman on 08.01.2016.
 */
public class YandexDiskPage {

    public static final By YANDEX_DISK_FOLDER_LOCATOR =
            By.xpath("//a[contains(@class,'item_disk')]");
    public static final By UPLOAD_BUTTON_LOCATOR =
            By.xpath("//input[@class = 'button__attach']");
    public static final By ALREADY_EXCISTS_LOCATOR =
            By.xpath("//span[@class = 'b-item-upload__error']");
    public static final By REPLACE_BUTTON_LOCATOR =
            By.xpath("//button[contains(@class,'replace')]");
    public static final By CLOSE_BUTTON_LOCATOR =
            By.xpath("//button[contains(@class,'button-close ns-action js-hide')]");
    public static final By UPLOAD_DONE_DIV_LOCATOR =
            By.xpath("//div[@class='b-item-upload__icon b-item-upload__icon_done']" +
                    "/parent::div");
    public static final By FILE_TO_TRASH_BUTTON_LOCATOR =
            By.xpath("//div[contains(@data-params,'/trash')]");
    public static final By PROGRESSBAR_LOCATOR =
            By.xpath("//div[@class = 'b-progressbar']");

    public static YandexTrashPage openTrash() {
        MyLogger.info("Open trash");
        Browser.current().click(YandexTrashPage.TO_TRASH_FOLDER_LOCATOR);
        return new YandexTrashPage();
    }

    public static void downloadFile(String fileName) {
        MyLogger.info("Download file " + fileName);
        By fileXpath = getFileXpath(fileName);
        Browser.current().click(fileXpath);
        Browser.current().click(getFileDownloadButton(fileName));
    }

    public static By getFileXpath(String fileName) {
        return By.xpath("//div[@title = '" + fileName + "' and @data-nb = 'resource']");
    }

    private static By getFileDownloadButton(String fileName) {
        return By.xpath("//button[@data-click-action='resource.download' " +
                "and contains(@data-params,'" + fileName + "')]");
    }

    public static void deleteFileToTrash(String fileName) {
        MyLogger.debug("Delete file to trash " + fileName);
        Browser.current().dragAndDropFile(getFileXpath(fileName),
                FILE_TO_TRASH_BUTTON_LOCATOR);
        Browser.current().waitUntilElementHide(PROGRESSBAR_LOCATOR);
    }

    public static void upload(String fileName) {
        MyLogger.info("Upload file : " + fileName);
        Browser.current().writeText(UPLOAD_BUTTON_LOCATOR,
                GlobalParameters.getInstance().getUploadPath() + fileName);
        if (Browser.current().elementIsOnThisPage(ALREADY_EXCISTS_LOCATOR)) {
            Browser.current().click(REPLACE_BUTTON_LOCATOR);
        }
        Browser.current().waitForElementIsClickable(UPLOAD_DONE_DIV_LOCATOR);
        Browser.current().click(CLOSE_BUTTON_LOCATOR);
    }

}

