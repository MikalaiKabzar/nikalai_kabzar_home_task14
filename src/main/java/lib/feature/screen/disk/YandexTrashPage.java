package lib.feature.screen.disk;

import lib.report.MyLogger;
import lib.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Shaman on 09.01.2016.
 */
public class YandexTrashPage extends YandexDiskPage {

    public static final By TO_TRASH_FOLDER_LOCATOR =
            By.xpath("//" + "div[contains(@class,'visible')]/div/" +
                    "a[@href = '/client/trash']");
    public static final By CLEAN_TRASH_BUTTON_LOCATOR =
            By.xpath("//button[@data-click-action='trash.clean']");
    public static final By EMPTY_TRASH_BUTTON_LOCATOR =
            By.xpath("//button[@data-click-action='trash.clean' " +
                    "and contains(@class,'ui-button-disabled')]");
    public static final By ACCEPT_CLEAN_TRASH_BUTTON_LOCATOR =
            By.xpath("//button[contains(@class,'confirmation-accept')]");
    public static final By NOTIFICATIONS_ITEM_MOVED_LOCATOR =
            By.xpath("//div[@class = 'notifications__item " +
                    "nb-island notifications__item_moved']");
    public static final By YANDEX_DISK_FILES_LOCATOR =
            By.xpath("//div[contains(@class,'visible')]/div[@data-id = 'disk']");

    public static void openFilesPage() {
        MyLogger.info("Open files page");
        Browser.current().click(YANDEX_DISK_FILES_LOCATOR);
    }

    private static By getFileDeletePermanentlyButton(String fileName) {
        return By.xpath("//div[contains(@class,'visible')]/div/div/div/" +
                "button[@data-click-action='resource.delete' and " +
                "contains(@data-params,'" + fileName + "')]");
    }

    private static By getFileRestoreButton(String fileName) {
        return By.xpath("//div[contains(@class,'visible')]/div/div/div/" +
                "button[@data-click-action='resource.restore' and " +
                "contains(@data-params,'" + fileName + "')]");
    }

    public static void deleteFilePermanently(String fileName) {
        MyLogger.debug("Delete file permanently " + fileName);
        Browser.current().click(getFileXpath(fileName));
        Browser.current().click(getFileDeletePermanentlyButton(fileName));
        Browser.current().waitUntilElementHide(PROGRESSBAR_LOCATOR);
    }

    public static void restoreFile(String fileName) {
        MyLogger.debug("Restore file " + fileName);
        Browser.current().click(getFileXpath(fileName));
        Browser.current().click(getFileRestoreButton(fileName));
        Browser.current().waitUntilElementHide(NOTIFICATIONS_ITEM_MOVED_LOCATOR);
    }

    public YandexTrashPage clearYandexDiskTrash() {
        MyLogger.info("Clear YandexDisk Trash");
        if (!Browser.current().elementIsOnThisPage(EMPTY_TRASH_BUTTON_LOCATOR)) {
            Browser.current().click(CLEAN_TRASH_BUTTON_LOCATOR);
            Browser.current().click(ACCEPT_CLEAN_TRASH_BUTTON_LOCATOR);
            Browser.current().refresh();
        }
        return this;
    }

}
