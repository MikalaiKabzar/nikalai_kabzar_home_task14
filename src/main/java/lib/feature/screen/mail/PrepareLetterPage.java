package lib.feature.screen.mail;

import lib.feature.models.letter.Letter;
import lib.ui.Browser;
import org.openqa.selenium.By;

import static lib.ui.Browser.screenshot;

/**
 * Created by Shaman on 04.01.2016.
 */
public class PrepareLetterPage extends BasePage {

    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']" +
            "//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By WRONG_ADDRESS_NAME_ERROR_LOCATOR =
            By.xpath("//span[@class = 'b-notification b-notification_error " +
                    "b-notification_error_invalid']");
    public static final By EMPTY_ADDRESS_ERROR_LOCATOR =
            By.xpath("//span[@class = 'b-notification b-notification_error " +
                    "b-notification_error_required']");
    public static final By DELETE_MESSAGE_LOCATOR =
            By.xpath("//*[class='b-statusline__content']");
    public static final By CONFIRM_SAVE_BUTTON_LOCATOR =
            By.xpath("//button[@data-action='dialog.save']");

    public static boolean checkEmptyAddressErrorOnPage() {
        screenshot();
        return Browser.current().elementIsOnThisPage(EMPTY_ADDRESS_ERROR_LOCATOR);
    }

    public static boolean checkWrongAddressErrorOnPage() {
        screenshot();
        return Browser.current().elementIsOnThisPage(WRONG_ADDRESS_NAME_ERROR_LOCATOR);
    }

    public static void confirmSaveDraft() {
        Browser.current().click(CONFIRM_SAVE_BUTTON_LOCATOR);
        Browser.current().elementIsOnThisPage(DraftPage.ADD_TEMPLATE_BUTTON_LOCATOR);
    }

    public static void sentMailButtonClick() {
        Browser.current().click(SEND_MAIL_BUTTON_LOCATOR);
    }

    public PrepareLetterPage prepareLetter(Letter letter) {
        Browser.current().writeTextClickable(TO_INPUT_LOCATOR, letter.getTo());
        Browser.current().writeTextClickable(SUBJECT_INPUT_LOCATOR, letter.getSubject());
        Browser.current().writeTextClickable(MAIL_TEXT_LOCATOR, letter.getBody());
        return this;
    }

}
