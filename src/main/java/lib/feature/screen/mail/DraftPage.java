package lib.feature.screen.mail;

import org.openqa.selenium.By;

/**
 * Created by Shaman on 05.01.2016.
 */
public class DraftPage extends BasePage {

    public static final By ADD_TEMPLATE_BUTTON_LOCATOR =
            By.xpath("//a[contains(@class,'add-template')]");

}
