package lib.feature.screen.mail;

import lib.feature.screen.disk.YandexDiskPage;
import lib.report.MyLogger;
import lib.ui.Browser;
import org.openqa.selenium.By;

import static lib.feature.screen.disk.YandexDiskPage.YANDEX_DISK_FOLDER_LOCATOR;
import static lib.feature.screen.mail.PrepareLetterPage.DELETE_MESSAGE_LOCATOR;
import static lib.ui.Browser.screenshot;

/**
 * Created by Shaman on 26.01.2016.
 */
public class BasePage {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By DRAFT_MAIL_FOLDER_LOCATOR = By.xpath("//a[@href = '#draft']");
    public static final By MAIL_INCOMING_FOLDER_LOCATOR = By.xpath("//a[@href = '#inbox']");
    public static final By MAIL_OUTCOMING_FOLDER_LOCATOR = By.xpath("//a[@href = '#sent']");
    public static final By TRASH_MAIL_FOLDER_LOCATOR = By.xpath("//a[@href = '#trash']");
    public static final By DONE_FRAME_LOCATOR =
            By.xpath("//div[@class = 'b-done b-done_promo']");
    public static final By STATUSLINE_HIDE_LOCATOR =
            By.xpath("//div[@class='b-statusline' and @style = 'display: none;']");
    public static final By HEADER_USER_NAME_LOCATOR =
            By.xpath("//span[contains(@class,'header-user-name')]");
    public static final By DELETE_LETTER_BUTTON_LOCATOR =
            By.xpath("//a[@data-action = 'delete']");
    public static final By FIRST_LETTER_LOCATOR =
            By.xpath("//div[@class='block-messages' and not(@style='display: none;')]" +
                    "//div[@class='b-messages b-messages_threaded']/div[1]");
    public static final By INCOMING_IS_OPEN_LOCATOR =
            By.xpath("//div[contains(@class,'inbox') and contains(@class,'folder_current')]");
    public static final By OUTCOMING_IS_OPEN_LOCATOR =
            By.xpath("//div[contains(@class,'sent') and contains(@class,'folder_current')]");
    public static final By DRAFT_IS_OPEN_LOCATOR =
            By.xpath("//div[contains(@class,'draft') and contains(@class,'folder_current')]");
    public static final By TRASH_IS_OPEN_LOCATOR =
            By.xpath("//div[contains(@class,'trash') and contains(@class,'folder_current')]");

    public static IncomingPage waitForDoneFrame() {
        MyLogger.info("Wait for done frame");
        Browser.current().elementIsOnThisPage(DONE_FRAME_LOCATOR);
        return new IncomingPage();
    }

    public static IncomingPage waitUntilStatuslineHide() {
        MyLogger.info("Wait for statusline hide");
        Browser.current().elementIsOnThisPage(STATUSLINE_HIDE_LOCATOR);
        return new IncomingPage();
    }

    public static IncomingPage openIncoming() {
        MyLogger.info("Open incoming");
        Browser.current().click(MAIL_INCOMING_FOLDER_LOCATOR);
        return new IncomingPage();
    }

    public static OutcomingPage openOutcoming() {
        MyLogger.info("Open outcoming");
        Browser.current().click(MAIL_OUTCOMING_FOLDER_LOCATOR);
        return new OutcomingPage();
    }

    public static DraftPage openDraft() {
        MyLogger.info("Open draft");
        Browser.current().click(DRAFT_MAIL_FOLDER_LOCATOR);
        return new DraftPage();
    }

    public static TrashPage openTrash() {
        MyLogger.info("Open trash");
        Browser.current().click(TRASH_MAIL_FOLDER_LOCATOR);
        return new TrashPage();
    }

    public static void deleteSelectedLetter() {
        MyLogger.debug("Delete current letter");
        Browser.current().click(DELETE_LETTER_BUTTON_LOCATOR);
        Browser.current().elementIsOnThisPage(DELETE_MESSAGE_LOCATOR);
    }

    public static YandexDiskPage openDisk() {
        MyLogger.info("Open disk");
        Browser.current().click(YANDEX_DISK_FOLDER_LOCATOR);
        return new YandexDiskPage();
    }

    public static PrepareLetterPage writeLetter() {
        Browser.current().click(COMPOSE_BUTTON_LOCATOR);
        return new PrepareLetterPage();
    }

}
