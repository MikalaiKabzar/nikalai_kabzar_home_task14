package lib.feature.screen;

import lib.report.MyLogger;
import lib.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginPage {

    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By LOGIN_FORM_LOCATOR = By.xpath("//a[contains(@href," +
            " 'mail.yandex')]");
    public static final By ERROR_MESSAGE_LOCATOR =
            By.xpath("//div[@class = 'error-msg']");
    public static final String WRONG_PASSWORD_ERROR_MESSAGE =
            "Неправильный логин или пароль.";
    public static final String NOEXISTENT_ACCOUNT_ERROR_MESSAGE =
            "Нет аккаунта с таким логином.";

    public static boolean checkErrorMessageWrongPasswordIsHere() {
        MyLogger.info("WRONG_PASSWORD_ERROR_MESSAGE '" + WRONG_PASSWORD_ERROR_MESSAGE
                +"', ERROR_MESSAGE '" + Browser.current().getText(ERROR_MESSAGE_LOCATOR)+"'");
        return Browser.current().getText(ERROR_MESSAGE_LOCATOR)
                .equals(WRONG_PASSWORD_ERROR_MESSAGE);
    }

    public static boolean checkErrorMessageNoexistentAccountIsHere() {
        MyLogger.info("NOEXISTENT_ACCOUNT_ERROR_MESSAGE '" + NOEXISTENT_ACCOUNT_ERROR_MESSAGE
                +"', ERROR_MESSAGE '" + Browser.current().getText(ERROR_MESSAGE_LOCATOR)+"'");
        return Browser.current().getText(ERROR_MESSAGE_LOCATOR)
                .equals(NOEXISTENT_ACCOUNT_ERROR_MESSAGE);
    }

    public LoginPage openLoginForm() {
        Browser.current().click(LOGIN_FORM_LOCATOR);
        return this;
    }

    public LoginPage clickSubmit() {
        Browser.current().submit(PASSWORD_INPUT_LOCATOR);
        return this;
    }

    public LoginPage setPassword(String password) {
        Browser.current().writeTextClickable(PASSWORD_INPUT_LOCATOR, password);
        return this;
    }

    public LoginPage setLogin(String login) {
        Browser.current().writeTextClickable(LOGIN_INPUT_LOCATOR, login);
        return this;
    }

    public LoginPage openUrl(String url) {
        Browser.current().open(url);
        return this;
    }

}
