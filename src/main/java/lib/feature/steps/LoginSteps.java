package lib.feature.steps;

import lib.feature.models.account.Account;
import lib.feature.models.account.AccountBuilder;
import lib.feature.screen.LoginPage;
import lib.feature.screen.mail.IncomingPage;
import lib.feature.services.LoginService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;
import tests.BaseTestClass;

import static lib.feature.services.LoginService.checkLoginPositive;

public class LoginSteps {

    public IncomingPage incomingPage = new IncomingPage();
    private Account actor;

    @Given("Actor is mailbox owner")
    public void buildCorrectAccount() {
        actor = AccountBuilder.getDefaultAccount();
    }

    @Given("Actor is mailbox owner. Actor forget correct password")
    public void buildWrongPasswordAccount() {
        actor = AccountBuilder.getWrongPasswordAccount();
    }

    @Given("Actor is not mailbox owner")
    public void buildNoExcistantAccount() {
        actor = AccountBuilder.getNonexistentAccount();
    }

    @When("Actor open browser")
    public void prepareBrowser() {
        BaseTestClass.prepareBrowser();
    }

    @When("Actor try to login and click submit")
    public void login() {
        LoginService.login(actor);
    }

    @Then("Actor's Mailbox page is opened")
    public void checkAccountMailboxOpened() {
        Assert.assertTrue(checkLoginPositive(actor), "Opened mailbox is not actor");
    }

    @Then("Verify that noexcistent account error is visible")
    public void checkNoExcistantAccountErrorIsVisible() {
        Assert.assertTrue(LoginPage.checkErrorMessageNoexistentAccountIsHere(), "There is no noExcistant account error message");
    }

    @Then("Verify that wrong password error message is visible")
    public void checkWrongPasswordErrorMessageIsVisible() {
        Assert.assertTrue(LoginPage.checkErrorMessageWrongPasswordIsHere(), "There is no wrong password error message");
    }

    @Then("Actor close browser")
    public void closeBrowser() {
        BaseTestClass.shutdownWebDriver();
    }
}
