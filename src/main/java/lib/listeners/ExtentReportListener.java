package lib.listeners;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import lib.feature.common.GlobalParameters;
import lib.report.MyLogger;
import org.apache.log4j.MDC;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static lib.listeners.IMdcContext.REPORT_METHOD;
import static lib.listeners.IMdcContext.REPORT_TEST;
import static lib.report.LoggerLevel.*;

/**
 * Created by Shaman on 03.02.2016.
 */
public class ExtentReportListener implements IResultListener2, IReporter {

    volatile static ExtentReports reports =
            new ExtentReports(GlobalParameters.getInstance().getResultDir()
                    + File.separator + "report.html", true);

    @Override
    public void beforeConfiguration(ITestResult result) {
        MyLogger.log(CONF_STARTED, result.getName());
        onTestStart(result);
    }

    @Override
    public void onConfigurationSuccess(ITestResult result) {
        MyLogger.log(CONF_SUCCESS, result.getName());
        onTestSuccess(result);
    }

    @Override
    public void onConfigurationFailure(ITestResult result) {
        MyLogger.log(CONF_FAILED, result.getName());
        onTestFailure(result);
    }

    @Override
    public void onConfigurationSkip(ITestResult result) {
        MyLogger.log(CONF_SKIPPED, result.getName());
        onTestSkipped(result);
    }

    @Override
    public void onTestStart(ITestResult result) {
        MyLogger.log(METHOD_STARTED, result.getName());
        ExtentTest test = (ExtentTest) MDC.get(REPORT_TEST);
        ExtentTest method = reports.startTest(result.getName());
        method.setStartedTime(new Date(System.nanoTime()));
        test.appendChild(method);
        MDC.put(REPORT_METHOD, method);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        MyLogger.log(METHOD_SUCCESS, result.getName());
        ExtentTest method = (ExtentTest) MDC.get(REPORT_METHOD);
        method.setEndedTime(new Date(System.nanoTime()));
        method.log(LogStatus.PASS, "green");
        reports.endTest(method);
        MDC.remove(REPORT_METHOD);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        MyLogger.log(METHOD_FAILED, result.getTestClass()
                .getRealClass().getSimpleName() + "."
                + result.getMethod().getMethodName(), result.getThrowable());
        ExtentTest method = (ExtentTest) MDC.get(REPORT_METHOD);
        method.setEndedTime(Calendar.getInstance().getTime());
        method.log(LogStatus.FAIL, "fail");
        method.log(LogStatus.FAIL, result.getThrowable());
        reports.endTest(method);
        MDC.remove(REPORT_METHOD);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        MyLogger.log(METHOD_SKIPPED, result.getName());
        ExtentTest method = (ExtentTest) MDC.get(REPORT_METHOD);
        method.setEndedTime(Calendar.getInstance().getTime());
        method.log(LogStatus.SKIP, "skip");
        reports.endTest(method);
        MDC.remove(REPORT_METHOD);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        // NO
    }

    @Override
    public void onStart(ITestContext context) {
        MyLogger.log(TEST_STARTED, context.getName());
        ExtentTest test = reports.startTest(context.getName());
        test.setStartedTime(Calendar.getInstance().getTime());
        MDC.put(REPORT_TEST, test);
    }

    @Override
    public void onFinish(ITestContext context) {
        MyLogger.log(TEST_FINISHED, context.getName());
        ExtentTest test = (ExtentTest) MDC.get(REPORT_TEST);
        test.setEndedTime(Calendar.getInstance().getTime());
        reports.endTest(test);
        MDC.remove(REPORT_TEST);
    }

    @Override
    public void generateReport(List<XmlSuite> xmlSuites,
                               List<ISuite> suites,
                               String outputDirectory) {
        reports.flush();
        reports.close();
    }

}
