package lib.listeners;

import lib.feature.common.GlobalParameters;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * Created by Shaman on 26.01.2016.
 */
public class ParallelSuitesListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalParameters.getInstance().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalParameters.getInstance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
