package lib.listeners;

/**
 * Created by Konstantsin_Simanenk on 1/27/2016.
 */
public interface IMdcContext {
    String REPORT_SUITE = "REPORT_SUITE";
    String REPORT_TEST = "REPORT_TEST";
    String REPORT_METHOD = "REPORT_METHOD";
}
