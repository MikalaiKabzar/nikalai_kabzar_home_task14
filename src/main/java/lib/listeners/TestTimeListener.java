package lib.listeners;

import lib.report.MyLogger;
import org.testng.IExecutionListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Shaman on 17.01.2016.
 */
public class TestTimeListener implements IExecutionListener {

    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private long startTime;

    @Override
    public void onExecutionStart() {
        startTime = System.currentTimeMillis();
        MyLogger.info("TestNG is going to start at " +
                format.format(Calendar.getInstance().getTime()));
    }

    @Override
    public void onExecutionFinish() {
        MyLogger.info("TestNG has finished at " +
                format.format(Calendar.getInstance().getTime()));
        MyLogger.info("Tests took around " +
                Math.round((System.currentTimeMillis() - startTime) / 1000) + " sec");
        MyLogger.info("___________________________________________________________"
                + "___________________________________________________________"
                + "___________________________________________________________");

    }
}

