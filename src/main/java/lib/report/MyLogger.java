package lib.report;

import org.apache.log4j.Priority;

/**
 * Created by Shaman on 22.01.2016.
 */
public class MyLogger {

    private static org.apache.log4j.Logger logger
            = org.apache.log4j.Logger.getLogger("homeLogger");

    public static void trace(String message, Throwable e) {
        logger.trace(message, e);
    }

    public static void trace(String message) {
        logger.trace(message);
    }

    public static void debug(String message, Throwable e) {
        logger.debug(message, e);
    }

    public static void debug(String message) {
        logger.debug(message);
    }

    public static void info(String message, Throwable e) {
        logger.info(message, e);
    }

    public static void info(String message) {
        logger.info(message);
    }

    public static void warn(String message, Throwable e) {
        logger.warn(message, e);
    }

    public static void warn(String message) {
        logger.warn(message);
    }

    public static void error(String message, Throwable e) {
        logger.error(message, e);
    }

    public static void error(String message) {
        logger.error(message);
    }

    public static void fatal(String message, Throwable e) {
        logger.fatal(message, e);
    }

    public static void fatal(String message) {
        logger.fatal(message);
    }

    public static void log(Priority priority, String message, Throwable t) {
        logger.log(priority, message, t);
    }

    public static void log(Priority priority, String message) {
        logger.log(priority, message);
    }

    public static void save(String pathToFile) {
        MyLogger.log(LoggerLevel.SAVE, pathToFile);
    }

}
