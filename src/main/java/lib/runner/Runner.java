package lib.runner;

import lib.feature.common.GlobalParameters;
import lib.listeners.ExtentReportListener;
import lib.listeners.ParallelSuitesListener;
import lib.listeners.TestTimeListener;
import lib.report.MyLogger;
import org.apache.commons.io.FileUtils;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.ITestNGListener;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shaman on 23.12.2015.
 */
public class Runner {

    public Runner(String[] args) {
        findArguments(args);
    }

    public static void main(String[] args) throws
            ParserConfigurationException, SAXException, IOException {
        new Runner(args).runTests();
    }

    private void runTests() throws
            IOException, SAXException, ParserConfigurationException {
        TestNG tng = new TestNG();
        addListeners(tng);
        configureSuites(tng);
        DeleteOldResultDirectory();
        MyLogger.info("Test will be started");
        tng.run();
    }

    private void DeleteOldResultDirectory() {
        FileUtils.deleteQuietly(
                new File(GlobalParameters.getInstance().getResultDir()));
    }

    private void findArguments(String[] args) {
        MyLogger.info("Parce Cli parameters");
        CmdLineParser parser = new CmdLineParser(GlobalParameters.getInstance());
        try {
            parser.parseArgument(args);
            MyLogger.info(GlobalParameters.getInstance().toString());
        } catch (CmdLineException e) {
            MyLogger.error("Failed to parce Cli parameters" + e.getMessage(), e);
            parser.printUsage(System.out);
            System.exit(1);
        }
    }

    private void addListeners(TestNG testNG) {
        testNG.setUseDefaultListeners(false);
        List<ITestNGListener> listeners = new ArrayList<ITestNGListener>() {{
            add(new ExtentReportListener());
            add(new TestTimeListener());
            add(new ParallelSuitesListener());
        }};
        for (ITestNGListener listener : listeners) {
            testNG.addListener(listener);
        }
    }

    private void configureSuites(TestNG testNG) throws
            ParserConfigurationException, SAXException, IOException {
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        for (String suitePath : GlobalParameters.getInstance().getSuites()) {
            InputStream suiteInClassPath = getSuiteInputStream(suitePath);
            if (suiteInClassPath != null) {
                suites.addAll(new Parser(suiteInClassPath).parse());
            } else {
                suites.addAll(new Parser(suitePath).parse());
            }
        }
        for (XmlSuite xmlSuite : suites) {
            testNG.setCommandLineSuite(xmlSuite);
        }
    }

    private InputStream getSuiteInputStream(String suite) {
        InputStream resourceAsStream = this.getClass().getClassLoader()
                .getResourceAsStream(suite);
        return resourceAsStream;
    }
}
