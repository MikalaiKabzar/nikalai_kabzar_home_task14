package tests.yandexDisk;

import lib.feature.services.DiskService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.BaseDiskTestClass;

/**
 * Created by Shaman on 10.01.2016.
 */
public class RemoveSeveralFilesCtrlClickTest extends BaseDiskTestClass {

    String[] fileName;

    @BeforeClass(description = "Prepare fileName for test")
    private void prepareFileName() {
        fileName = new String[]{getFileName(1), getFileName(2)};
    }

    @BeforeMethod(description = "Prepare Y.Disk files and folders")
    public void prepareYDiskFilesFolders() {
        DiskService.prepareYandexDiskFilesFolders(getFilesForTest());
    }

    @Test(description = "Remove several files with 'ctrl+click' test")
    public void realizeRemoveSeveralFilesCtrlClick() {
        DiskService.deleteFilesCtrlClick(fileName);
        Assert.assertTrue(DiskService.checkFilesNotHere(fileName),
                THERE_ARE_FILES_IN_DISK_ERROR_MESSAGE);
        Assert.assertTrue(DiskService.checkFilesInTrash(fileName),
                THERE_ARE_NO_FILES_IN_TRASH_ERROR_MESSAGE);
    }
}
