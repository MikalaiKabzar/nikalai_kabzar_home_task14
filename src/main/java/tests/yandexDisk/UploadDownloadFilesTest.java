package tests.yandexDisk;

import lib.feature.services.DiskService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.BaseDiskTestClass;

/**
 * Created by Shaman on 24.12.2015.
 */
public class UploadDownloadFilesTest extends BaseDiskTestClass {

    String[] fileName;

    @BeforeClass(description = "Prepare fileName for test")
    private void prepareFileName() {
        fileName = new String[]{getFileName(1)};
    }

    @Test(description = "Upload test")
    public void realizeUploadFiles() {
        DiskService.uploadFile(fileName);
        Assert.assertTrue(DiskService.checkFilesHere(fileName),
                THERE_ARE_NO_FILES_IN_DISK_ERROR_MESSAGE);
    }

    @Test(description = "Download test")
    public void realizeDownloadFiles() {
        DiskService.prepareYandexDiskFilesFolders(getFilesForTest());
        DiskService.downloadFile(fileName);
        Assert.assertTrue(DiskService.checkCorrectDownloadFile(fileName),
                "Error. There are no downloaded files in download folder");
        Assert.assertTrue(DiskService.checkDownloadedFile(fileName),
                "Error. The downloaded files have incorrect content");
    }
}
