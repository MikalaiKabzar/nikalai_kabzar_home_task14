package tests.yandexDisk;

import lib.feature.services.DiskService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.BaseDiskTestClass;

/**
 * Created by Shaman on 09.01.2016.
 */
public class FilesDeletePermanentlyTest extends BaseDiskTestClass {

    String[] fileName;

    @BeforeClass(description = "Prepare fileName for test")
    private void prepareFileName() {
        fileName = new String[]{getFileName(1)};
    }

    @BeforeMethod(description = "Prepare Y.Disk files and folders")
    public void prepareYDiskFilesFolders() {
        DiskService.prepareYandexDiskFilesFolders(getFilesForTest());
        DiskService.deleteFilesToTrash(fileName);
    }

    @Test(description = "Delete files permanently test")
    public void deleteFilesPermanently() {
        Assert.assertTrue(DiskService.checkFilesInTrash(fileName),
                THERE_ARE_NO_FILES_IN_TRASH_ERROR_MESSAGE);
        DiskService.deleteFilesPermanently(fileName);
        Assert.assertTrue(DiskService.checkFilesNotHere(fileName),
                THERE_ARE_FILES_IN_TRASH_ERROR_MESSAGE);
    }

}
