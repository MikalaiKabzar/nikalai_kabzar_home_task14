package tests.yandexMail;

import lib.feature.models.letter.Letter;
import lib.feature.models.letter.LetterBuilder;
import lib.feature.services.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseMailTestClass;

/**
 * Created by Shaman on 27.12.2015.
 */
public class EmptyLetterSentTest extends BaseMailTestClass {

    Letter letter = LetterBuilder.getEmptyContentLetter();

    @Test(description = "Mail empty letter sent test")
    public void realizeEmptyLetterSent() {
        MailService.sentLetter(letter);
        Assert.assertTrue(MailService.checkEmptyLetterIsHere(letter),
                THERE_IS_NO_LETTER_IN_INBOX_ERROR_MESSAGE);
        Assert.assertTrue(MailService.checkEmptyLetterInOutBox(letter),
                THERE_IS_NO_LETTER_IN_OUTBOX_ERROR_MESSAGE);
    }
}
