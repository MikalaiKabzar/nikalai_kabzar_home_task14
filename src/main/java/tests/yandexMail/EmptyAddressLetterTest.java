package tests.yandexMail;

import lib.feature.models.letter.Letter;
import lib.feature.models.letter.LetterBuilder;
import lib.feature.screen.mail.PrepareLetterPage;
import lib.feature.services.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseMailTestClass;

/**
 * Created by Shaman on 27.12.2015.
 */
public class EmptyAddressLetterTest extends BaseMailTestClass {

    Letter letter = LetterBuilder.getEmptyAddressLetter();

    @Test(description = "Empty address letter test")
    public void realizeEmptyAddressLetter() {
        MailService.trySentLetter(letter);
        Assert.assertTrue(PrepareLetterPage.checkEmptyAddressErrorOnPage(),
                "Error. There is no empty address error message on the page");
    }
}

