package tests.yandexMail;

import lib.feature.models.letter.Letter;
import lib.feature.models.letter.LetterBuilder;
import lib.feature.services.MailService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.BaseMailTestClass;

/**
 * Created by Shaman on 28.12.2015.
 */
public class DraftLetterTrashDeletedTest extends BaseMailTestClass {

    Letter letter = LetterBuilder.getRandomLetter();

    @BeforeMethod
    private void prepareDraftLetter() {
        MailService.makeDraftLetter(letter);
    }

    @Test(description = "Create draft letter test")
    public void realizeDraftLetter() {
        prepareDraftLetter();
        Assert.assertTrue(MailService.checkLetterIsHere(letter),
                "Error. There is no letter in the draft.");
    }

    @Test(description = "Delete to trash, permanently delete draft letter test")
    public void realizeDeleteDraftLetter() {
        prepareDraftLetter();
        MailService.deleteLetter(letter);
        Assert.assertFalse(MailService.checkLetterIsHere(letter),
                "Error. There is letter in the draft.");
        Assert.assertTrue(MailService.checkLetterIsInTrash(letter),
                THERE_IS_NO_LETTER_IN_TRASH_ERROR_MESSAGE);
        MailService.deleteLetter(letter);
        Assert.assertFalse(MailService.checkLetterIsHere(letter),
                THERE_IS_LETTER_IN_TRASH_ERROR_MESSAGE);
    }

}
