package tests.yandexMail;

import lib.feature.models.letter.Letter;
import lib.feature.models.letter.LetterBuilder;
import lib.feature.screen.mail.PrepareLetterPage;
import lib.feature.services.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseMailTestClass;

/**
 * Created by Shaman on 27.12.2015.
 */
public class WrongAddressLetterTest extends BaseMailTestClass {

    Letter letter = LetterBuilder.getWrongAddressLetter();

    @Test(description = "Wrong address letter test")
    public void realizeWrongAddressLetter() {
        MailService.trySentLetter(letter);
        Assert.assertTrue(PrepareLetterPage.checkWrongAddressErrorOnPage(),
                "Error. There is no wrong address error message on the page");
    }
}
