package tests.yandexMail;

import lib.feature.models.letter.Letter;
import lib.feature.models.letter.LetterBuilder;
import lib.feature.services.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseMailTestClass;


/**
 * Created by Shaman on 24.12.2015.
 */
public class FilledLetterSentTest extends BaseMailTestClass {

    Letter letter = LetterBuilder.getRandomLetter();

    @Test(description = "Mail filled send test")
    public void realizeFilledLetterSent() {
        MailService.sentLetter(letter);
        MailService.findLetterId(letter);
        Assert.assertTrue(MailService.checkCurrentLetterIsHere(letter),
                THERE_IS_NO_LETTER_IN_INBOX_ERROR_MESSAGE);
        Assert.assertTrue(MailService.checkCurrentLetterInOutBox(letter),
                THERE_IS_NO_LETTER_IN_OUTBOX_ERROR_MESSAGE);
    }
}
