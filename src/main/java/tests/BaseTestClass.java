package tests;

import lib.ui.Browser;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static lib.ui.Browser.screenshot;

/**
 * Created by Shaman on 05.01.2016.
 */

public class BaseTestClass {

    @BeforeMethod
    public static void prepareBrowser() {
        Browser.rise();
    }

    @AfterMethod
    public static void shutdownWebDriver() {
        screenshot();
        Browser.current().close();
    }

}

