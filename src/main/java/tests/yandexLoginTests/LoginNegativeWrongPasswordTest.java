package tests.yandexLoginTests;

import lib.feature.models.account.Account;
import lib.feature.models.account.AccountBuilder;
import lib.feature.screen.LoginPage;
import lib.feature.services.LoginService;
import lib.report.MyLogger;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTestClass;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginNegativeWrongPasswordTest extends BaseTestClass {

    Account account = AccountBuilder.getWrongPasswordAccount();

    @Test(description = "Login negative test with wrong password")
    public void realizeLoginNegativeWrongPasswordTest() {
        MyLogger.debug("Try to login (login: " + account.getLogin() +
                ", password: " + account.getPassword() + ")");
        LoginService.login(account);
        Assert.assertTrue(LoginPage.checkErrorMessageWrongPasswordIsHere(),
                "Error. Account must contain correct login and incorrect password");
    }
}
