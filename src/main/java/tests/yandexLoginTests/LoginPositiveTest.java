package tests.yandexLoginTests;

import lib.feature.models.account.Account;
import lib.feature.models.account.AccountBuilder;
import lib.feature.services.LoginService;
import lib.report.MyLogger;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTestClass;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginPositiveTest extends BaseTestClass {

    Account account = AccountBuilder.getDefaultAccount();

    @Test(description = "Login positive test. " +
            "Login to MailBox with correct login and password")
    public void realizeLoginPositiveTest() {
        MyLogger.debug("Try to login (login: " + account.getLogin() +
                ", password: " + account.getPassword() + ")");
        LoginService.login(account);
        Assert.assertTrue(LoginService.checkLoginPositive(account),
                "Error. Account must contain correct login and password");
    }
}
