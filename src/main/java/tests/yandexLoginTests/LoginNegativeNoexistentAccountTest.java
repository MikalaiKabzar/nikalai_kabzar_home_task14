package tests.yandexLoginTests;

import lib.feature.models.account.Account;
import lib.feature.models.account.AccountBuilder;
import lib.feature.screen.LoginPage;
import lib.feature.services.LoginService;
import lib.report.MyLogger;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTestClass;

/**
 * Created by Shaman on 17.01.2016.
 */
public class LoginNegativeNoexistentAccountTest extends BaseTestClass {

    Account account = AccountBuilder.getNonexistentAccount();

    @Test(description = "Login negative test with wrong password")
    public void realizeLoginNegativeNoexistentAccountTest() {
        MyLogger.debug("Try to login (login: " + account.getLogin() +
                ", password: " + account.getPassword() + ")");
        LoginService.login(account);
        Assert.assertTrue(LoginPage.checkErrorMessageNoexistentAccountIsHere(),
                "Error. You should use noexistent account");
    }
}
