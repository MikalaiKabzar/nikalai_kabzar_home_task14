package tests;

import lib.feature.models.account.AccountBuilder;
import lib.feature.services.LoginService;
import org.testng.annotations.BeforeMethod;

/**
 * Created by Shaman on 08.02.2016.
 */
public class BaseMailTestClass extends BaseTestClass {

    public static final String THERE_IS_NO_LETTER_IN_INBOX_ERROR_MESSAGE
            = "Error. There is no letter in the inbox";
    public static final String THERE_IS_NO_LETTER_IN_OUTBOX_ERROR_MESSAGE
            = "Error. There is no letter in the outbox";
    public static final String THERE_IS_LETTER_IN_TRASH_ERROR_MESSAGE
            = "Error. There is letter in the trash";
    public static final String THERE_IS_NO_LETTER_IN_TRASH_ERROR_MESSAGE
            = "Error. There is no letter in the trash";

    @BeforeMethod
    public void loginYandex() {

        LoginService.login(AccountBuilder.getDefaultAccount());
    }
}
