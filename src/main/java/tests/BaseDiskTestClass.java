package tests;

import lib.feature.common.GlobalParameters;
import lib.feature.models.textFile.TextFile;
import lib.feature.models.textFile.TextFileBuilder;
import lib.report.MyLogger;
import lib.util.Content;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shaman on 31.01.2016.
 */
public class BaseDiskTestClass extends BaseMailTestClass {

    public static final String THERE_ARE_FILES_IN_DISK_ERROR_MESSAGE
            = "Error. There are files in the YandexDisk";
    public static final String THERE_ARE_NO_FILES_IN_DISK_ERROR_MESSAGE
            = "Error. There are no files in the YandexDisk";
    public static final String THERE_ARE_FILES_IN_TRASH_ERROR_MESSAGE
            = "Error. There are files in the Yandex/Trash";
    public static final String THERE_ARE_NO_FILES_IN_TRASH_ERROR_MESSAGE
            = "Error. There are no files in the Yandex/Trash";

    public static List<TextFile> filesForTest = new ArrayList<TextFile>();

    public static List<TextFile> getFilesForTest() {
        return filesForTest;
    }

    public static String getFileName(int nomber) {
        return filesForTest.get(nomber - 1).getName();
    }

    @BeforeTest
    public void prepateFilesFolders() {
        MyLogger.info("Prepare files for test.");
        new File(GlobalParameters.getInstance().getUploadPath()).mkdirs();
        new File(GlobalParameters.getInstance().getDownloadPath()).mkdirs();
        filesForTest.add(tryCreateTestTextFile(TextFileBuilder.getTextFile(1)));
        filesForTest.add(tryCreateTestTextFile(TextFileBuilder.getTextFile(2)));
        Content.fillContentMap(GlobalParameters.getInstance().getUploadPath(), "*.txt");
    }

    @AfterTest
    public void deleteTempFolder() {
        try {
            FileUtils.forceDeleteOnExit(
                    new File(GlobalParameters.getInstance().getTempFolderPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        MyLogger.info("Delete temp folder Disk'"
                + GlobalParameters.getInstance().getTempFolderPath() + "'");
    }

    protected TextFile tryCreateTestTextFile(TextFile textFile) {
        String fileName = textFile.getAddress() + textFile.getName();
        try {
            OutputStreamWriter file = new OutputStreamWriter(
                    new FileOutputStream(fileName), "KOI8-R");
            file.write(textFile.getContent());
            file.close();
            if (!new File(fileName).exists()) {
                MyLogger.error("There is no file: " + fileName);
            } else {
                MyLogger.info("There is file: " + fileName);
            }
        } catch (IOException e) {
            MyLogger.error("Failed to create test text file : " + e.getMessage(), e);
            e.printStackTrace();
        }
        return textFile;
    }

}
