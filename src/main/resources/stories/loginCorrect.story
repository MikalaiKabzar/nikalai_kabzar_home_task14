Meta:

Narrative:
As a mailbox user
I want to login to mailbox with correct credentails
So that I can get access to my mailbox page

Scenario: Login into mailbox with existent user
Given Actor is mailbox owner
When Actor open browser
When Actor try to login and click submit
Then Actor's Mailbox page is opened
Then Actor close browser