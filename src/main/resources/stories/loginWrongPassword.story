Meta:

Narrative:
As a mailbox user who has forgotten correct password
I want to login to mailbox with wrong password
So that I can see wrong password error message

Scenario: Login into mailbox with existent user
Given Actor is mailbox owner. Actor forget correct password
When Actor open browser
When Actor try to login and click submit
Then Verify that wrong password error message is visible
Then Actor close browser